<?php
/**
 * Header Template
 * @file           header.php
 * @package        Horsebox Services
 * @filesource     wp-content/themes/horseboxservices/header.php
 * @since          Horsebox Services 1.0
*/
?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="no-js ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<?php
 if (!is_admin()) {
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false);
	wp_enqueue_script('jquery');
}
?>
<script type="text/javascript">
// initialise Superfish
$(document).ready(function(){
  $("ul.sf-menu").superfish({
animation: {opacity:'show',height:'show'}, // fade-in and slide-down animation
delay: 500, // 0.5 second delay on mouseout
speed: 'normal'
  });
});
</script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<meta name="google-site-verification" content="W6y1CeYrRml7L_hh_pvSoKX3RgaIpvyXRDQaVVkaXm8" />
	<meta name="msvalidate.01" content="487F3FCB297013C9BD55D36049C3FC58" />
	<script>
     
    </script> 
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:700|Roboto:400,700" rel="stylesheet"> 
</head>
<body>
	<div class="sitewrapper">
    	<?php if (get_field('contact_number','option') || get_field('contact_email','option') || get_field('facebook','option') || get_field('twitter','option') || get_field('snapchat','option') || get_field('instagram','option') || get_field('google_plus','option') ) { ?>
            <div class="topbar wrapper">
                <div class="content">
                    <?php if (get_field('contact_number','option')) { ?>
                        <?php $phone = get_field('contact_number','option'); ?>
                        <?php $countryCode = '44'; ?>
                        <?php $tel = preg_replace("/[^0-9]/", "", $phone); ?>
                        <?php $tel = preg_replace('/^0?/', '+'.$countryCode, $tel); ?>
                        <a class="phone" href="tel:<?php echo $tel ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php the_field('contact_number','option') ?></a>
                    <?php } ?>
                    <?php if (get_field('contact_email','option')) { ?>
                        <a class="email" href="mailto:<?php the_field('contact_email','option') ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php the_field('contact_email','option') ?></a>
                    <?php } ?>
					
                </div>
            </div>
       	<?php } ?>
    	<div class="header wrapper">
        	<div class="content">
            	<div class="flexwrapper alignmiddle spacebetween">
                	<div class="site-logo">
						<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                        ?>
                        <?php if ( $custom_logo_id ) { ?>
                            <div class="logo">
                                <a href="<?php echo get_home_url(); ?>" rel="home"><img src="<?php echo $image[0]; ?>" alt="<?php echo get_bloginfo('name');?> - <?php echo get_bloginfo('description'); ?>"/></a>
                            </div>
                        <?php } else { ?>
                            <h2 class="site-title"><a href="<?php echo get_home_url(); ?>" rel="home"><?php echo get_bloginfo('name');?></a></h2><br/>
                            <span class="site-description"><?php echo get_bloginfo('description'); ?></span>
                        <?php } ?>
    				</div>
                    <nav class="mainnavmenu">
                        <?php wp_nav_menu( array(
							'container' => 'nav',
							'container_class' => 'header-nav',
							'menu_class' => 'sf-menu',
							'theme_location' => 'main'));?>
                    </nav>
                </div>
            </div>
        </div>
    