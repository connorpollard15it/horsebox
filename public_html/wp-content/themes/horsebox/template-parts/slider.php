	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
    ?>
    <?php if (get_sub_field( 'anchor' ) ) { ?>
        <a id="<?php the_sub_field( 'anchor' ); ?>"></a>
    <?php } ?>
        <?php if ( $rgba ) { ?>
            <div class="gallery-slider <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
        <?php } else { ?>
            <div class="section gallery-slider <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
            <?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
        <?php } ?>
                <?php if (get_sub_field('section_title')) { ?> 
                <div class="content">
                    <h2 class="section-title"<?php if (get_sub_field('text_colour')) { ?>style="color:<?php the_sub_field('text_colour');?>;"<?php }?>><?php the_sub_field('section_title');?></h2>
                </div>
                <?php } ?>
                <?php $images = get_sub_field('gallery_images'); ?>
                <?php $size = 'full'; ?>
                <?php if( $images ){ ?>
                    <div class="flexslider">
                        <ul class="slides">
                            <?php foreach( $images as $image ) { ?>
                                <?php //print_r ($image) ; ?>
                                    <li><?php echo wp_get_attachment_image( $image['ID'], $size ); ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php if (get_sub_field('link_to_gallery_page')) { ?>
                        <a class="button" href="<?php the_sub_field('link_to_gallery_page');?>"><?php if (get_sub_field('link_to_button_text')) { ?><?php the_sub_field('link_to_button_text');?><?php } else { ?>See More<?php } ?></a>
                    <?php } ?>
                <?php } ?>
				</div>