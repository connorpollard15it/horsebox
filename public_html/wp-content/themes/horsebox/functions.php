<?php
/**
 * Functions Template
 * @file           functions.php
 * @package        Horsebox Services
 * @filesource     wp-content/themes/horseboxservices/functions.php
 * @since          Horsebox Services 1.0
*/
 
add_action( 'after_setup_theme', 'horsebox_theme_setup' );

//Defines what theme_setup does

function horsebox_theme_setup() {

	$defaults = array(
		'width' => '1920',
		'height' => '600',
		'flex-height' => true,
		'flex-width' => true,
	);
	
	add_theme_support( 'custom-header', $defaults );

		add_theme_support( 'custom-logo', array(
		'height'      => 120,
		'width'       => 120,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );
	
	function horsebox_the_custom_logo() {
		
		if ( function_exists( 'the_custom_logo' ) ) {
			the_custom_logo();
		}
	
	}
	
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'banner', 1920, 600, true );
	
	function horsebox_register_menus() {

	/* Register nav menus using register_nav_menu() or register_nav_menus() here. */

		  register_nav_menus(
			array(
			  'main' => __( 'Main Menu' ),
			)
		  );
		  register_nav_menus(
			array(
			  'footer' => __( 'Footer Menu' ),
			)
		  );
		  
	}
	add_action( 'init', 'horsebox_register_menus' );

// custom post type

function register_cpt_services() {

    $labels = array( 
        'name' => _x( 'Services', 'services' ),
        'singular_name' => _x( 'Services', 'services' ),
        'add_new' => _x( 'Add New services', 'services' ),
        'add_new_item' => _x( 'Add New services', 'services' ),
        'edit_item' => _x( 'Edit services', 'services' ),
        'new_item' => _x( 'New services', 'services' ),
        'view_item' => _x( 'View services', 'services' ),
        'search_items' => _x( 'Search services', 'services' ),
        'not_found' => _x( 'No services found', 'services' ),
        'not_found_in_trash' => _x( 'No menu items found in Trash', 'services' ),
        'parent_item_colon' => _x( 'Parent services:', 'services' ),
        'menu_name' => _x( 'Services', 'services' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'services Files',
        'supports' => array( 'title', 'thumbnail', 'revisions', 'editor', 'page-attributes' ),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array( 'slug' => 'services' ),
        'capability_type' => 'post',
		'has_archive' => true,
		
		//'register_meta_box_cb' => 'add_summary_metaboxes'

    );

    register_post_type( 'services', $args );
}
add_action( 'init', 'register_cpt_services', 0 );

// End of theme set up
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
 

/* Convert hexdec color string to rgb(a) string */
				function hex2rgba($color, $opacity = false) {
				 
					$default = 'rgb(0,0,0)';
				 
					//Return default if no color provided
					if(empty($color))
						  return $default; 
				 
					//Sanitize $color if "#" is provided 
						if ($color[0] == '#' ) {
							$color = substr( $color, 1 );
						}
				 
						//Check if color has 6 or 3 characters and get values
						if (strlen($color) == 6) {
								$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
						} elseif ( strlen( $color ) == 3 ) {
								$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
						} else {
								return $default;
						}
				 
						//Convert hexadec to rgb
						$rgb =  array_map('hexdec', $hex);
				 
						//Check if opacity is set(rgba or rgb)
						if($opacity){
							if(abs($opacity) > 1)
								$opacity = 1.0;
							$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
						} else {
							$output = 'rgb('.implode(",",$rgb).')';
						}
				 
						//Return rgb(a) color string
						return $output;
				}
				
//Shortcode to display post loop				
// create shortcode with parameters so that the user can define what's queried - default is to list all blog posts
add_shortcode( 'list-posts', 'rmcc_post_listing_parameters_shortcode' );
function rmcc_post_listing_parameters_shortcode( $atts ) {
    ob_start();
 
    // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'post',
        'order' => 'date',
        'orderby' => 'title',
        'posts' => 10,
        'category' => '',
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => $type,
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => $posts,
        'category_name' => $category,
    );
    $query = new WP_Query( $options );
    // run the loop based on the query
    if ( $query->have_posts() ) { ?>
        <div class="post-listing <?php echo $type ?>-listing">
    	<?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <li id="post-<?php the_ID(); ?>" class="<?php echo get_post_type(); ?> revealup"><a href="<?php the_permalink();?>" rel="bookmark">
            	<?php if (has_post_thumbnail()) { ?>
                	<div class="services-thumb">
                		<?php the_post_thumbnail('medium');?>
                    </div>
                <?php }?>
                <div class="<?php echo $type ?>-excerpt">
                    <div class="<?php echo $type ?>-excerpt-content">
                        <h4 class="entry-title"><?php the_title();?></h4>
                        <?php if (get_field('excerpt')) { ?>
                            <?php the_field('excerpt');?>
                        <?php } ?>
                  	</div>
              	</div>
            </a></li>
		<?php
           /* $myvariable = ob_get_clean();
            return $myvariable;*/
		} ?>
        </div>
    <?php }
	wp_reset_postdata();

}

add_shortcode('show-stats', 'bacon_acf_stats_shortcode');
function bacon_acf_stats_shortcode() {
	ob_start();
    	if( have_rows ('statistics','options') ) {?>
        	<div class="flexwrapper flex-wrap">
        	<?php while ( have_rows ('statistics','options') ) { the_row();?>
				<div class="stats">
                    <?php if (get_sub_field( 'stat_value' ) ) { ?>
                        <div class="stat-value" data-content="<?php the_sub_field( 'stat_value' ); ?>" data-num="<?php the_sub_field( 'stat_value' ); ?>">0</div>
                    <?php } ?>
					<?php if (get_sub_field( 'stat_name' ) ) { ?> 
                        <span class="stat-title"><?php the_sub_field( 'stat_name' ); ?></span>
                    <?php } ?>
              	</div>
			<?php } ?>
			</div>
		<?php }
		$myvariable = ob_get_clean();
	return $myvariable;
}


//Create function to automatically update copyright date
function wpb_copyright() {
	global $wpdb;
	$copyright_dates = $wpdb->get_results("
		SELECT
		YEAR(min(post_date_gmt)) AS firstdate,
		YEAR(max(post_date_gmt)) AS lastdate
		FROM
		$wpdb->posts
		WHERE
		post_status = 'publish'
	");
	$output = '';
	if($copyright_dates) {
		$copyright = "© " . $copyright_dates[0]->firstdate;
	if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
		$copyright .= '-' . $copyright_dates[0]->lastdate;
	}
		$output = $copyright;
	}
	return $output;
}

/**
* Register and load font awesome CSS files using a CDN.
*
* @link http://www.bootstrapcdn.com/#fontawesome
* @author FAT Media

*/
	//enqueues our locally supplied font awesome stylesheet
	function enqueue_our_required_stylesheets(){
		wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.0.9/css/all.css'); 
	}
	add_action('wp_enqueue_scripts','enqueue_our_required_stylesheets');

/*----- Add in JS support for lightbox -----*/
	add_action( 'wp_enqueue_scripts', 'lightbox' );
	
	function lightbox() {
		wp_register_script( 'lightbox', get_template_directory_uri() . '/inc/js/fancybox-master/dist/jquery.fancybox.min.js', array('jquery') );
		wp_enqueue_script( 'lightbox' );
		wp_register_style( 'lightbox', get_template_directory_uri() . '/inc/js/fancybox-master/dist/jquery.fancybox.min.css' );
		wp_enqueue_style('lightbox');
		wp_register_script( 'countup', get_template_directory_uri() . '/inc/js/countup.js', array('jquery') );
		wp_enqueue_script( 'countup' );
		wp_register_script( 'flexslider', get_template_directory_uri() . '/inc/js/flexslider/flexslider.js' );
		wp_register_style( 'flexslider', get_template_directory_uri() . '/inc/js/flexslider/flexslider.css');
			wp_enqueue_script( 'flexslider' );
			wp_enqueue_style( 'flexslider' );
	}
	
	add_action( 'wp_enqueue_scripts', 'animate' );
	function superfish() {
		wp_register_script('hoverIntent', get_bloginfo('template_directory') . '/includes/hoverIntent.js', null, null, false);
		wp_enqueue_script('hoverIntent');
		wp_register_script('superfish', get_bloginfo('template_directory') . '/includes/superfish.js', null, null, false);
		wp_enqueue_script('superfish');
	}
	function animate() {
		wp_register_script( 'animate', get_template_directory_uri() . '/inc/js/animate.js','' ,'' ,'true');
		wp_enqueue_script( 'animate' ); 
	}
	