<?php
/**
 * Index Template
 * @file           single-portfolio.php
 * @package        Vampires Rock
 * @filesource     wp-content/themes/vampires/single-portfolio.php
 * @since          Vampires Rock 1.0
*/
//if (is_home() || is_front_page()) {
//	get_header('home');
//} else {
	get_header();
//}
;?>
	<div id="content-<?php the_ID();?>" class="body wrapper">  
		<?php if (have_posts()) { ?>
            <?php while (have_posts()) { ?>
            	<?php the_post();?>
					<?php $type = get_post_type();?>
                    <div class="content">
                        <article class="portfolio">
                        
							<?php if (has_post_thumbnail()) { ?>
                                <div class="single-<?php echo $type;?>-thumb">
                                    <a href="<?php the_permalink();?>">
                                        <?php the_post_thumbnail('large');?>
                                    </a>
                                </div>
                            <?php }?>
                            <div class="single-<?php echo $type;?>-content">
                                <h1 class="entry-title single-<?php echo $type;?>-title"><a href="<?php the_permalink();?>">
                                    <?php the_title();?>
                                </a></h1>
                                <?php if (get_field('about_the_customer')) { ?>
                                    <div class="about-customer portfolio-content-section">
                                    	<h3 class="about-customer-title">About the customer</h3>
                                        <?php the_field('about_the_customer');?>
                                    </div>
                                <?php } ?>
                                <?php if (get_field('work_carried_out')) { ?>
                                    <div class="about-work portfolio-content-section">
                                    	<h3 class="about-work-title">What work we've done</h3>
                                        <?php the_field('work_carried_out');?>
                                    </div>
                                <?php } ?>
                                <?php if (get_field('testimonial')) { ?>
                                    <div class="customer-testimonial portfolio-content-section">
                                        <blockquote>
                                            <?php the_field('testimonial');?>
											<?php if (get_field('testimonial_by')) { ?>
                                                <h4 class="testimonial-by">- <?php the_field('testimonial_by');?></h4>
                                            <?php } ?>
                                        </blockquote>
                                    </div>
                                <?php } ?>
                                
                                <?php if (get_field('link_to_website') || get_field('link_to_facebook') || get_field('link_to_twitter') || get_field('link_to_google_plus') || get_field('link_to_instagram') ) { ?>
                                    <div class="portfolio-social">
                                    	<h3 class="customer-social-title">Find them here:</h3>
                                        <?php if (get_field('link_to_website')) { ?>
                                            <a href="<?php the_field('link_to_website');?>" target="_blank"><i class="fas fa-globe"></i></a>
                                        <?php } ?>
                                        <?php if (get_field('link_to_facebook')) { ?>
                                            <a href="<?php the_field('link_to_facebook');?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                        <?php } ?>
                                        <?php if (get_field('link_to_twitter')) { ?>
                                            <a href="<?php the_field('link_to_twitter');?>" target="_blank"><i class="fab fa-twitter"></i></a>
                                        <?php } ?>
                                        <?php if (get_field('link_to_google_plus')) { ?>
                                            <a href="<?php the_field('link_to_google_plus');?>" target="_blank"><i class="fab fa-google-plus-g"></i></a>
                                        <?php } ?>
                                        <?php if (get_field('link_to_instagram')) { ?>
                                            <a href="<?php the_field('link_to_instagram');?>" target="_blank"><i class="fab fa-instagram"></i></a> 
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                             	</div>
                            </article>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
	</div>
<?php get_footer();?>