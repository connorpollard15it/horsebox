<?php
/**
 * Index Template
 * @file           index.php
 * @package        Horsebox Services
 * @filesource     wp-content/themes/horseboxservices/index.php
 * @since          Horsebox Services 1.0
*/
get_header();?>
    	<div id="wrapper" class="pagepiling"> <!-- this is used to enable the side scroll -->
		<?php if (have_posts()) { ?>
            <?php while (have_posts()) { ?>
            	<?php the_post();?>
                	<?php if ( !empty( get_the_content() ) ) { ?>
                    <div class="section" data-anchor="main-content">
                    	<!--<div class="pp-tableCell" style="height:100%">-->
                            <div class="content">
                                <article class="page">
                                    <h1 class="page-title"><a class="page-link" rel="bookmark" href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                                        <?php the_content();?>
                                </article>
                            </div>
                      	<!--</div>-->
                    </div>
                    <?php } ?>
					<?php if( have_rows ('page_content') ) { ?>
                        <?php while ( have_rows ('page_content') ) { the_row(); ?>
                            <?php if( get_row_layout() == 'full_image_no_text' ) { //Full Image No text ?>
                            
                                <?php get_template_part( 'template-parts/full_image' ); ?>
                                
                            <?php } else if ( get_row_layout() == 'full_image_with_text' ) { //Full Image With Text ?>
                                
                                <?php get_template_part( 'template-parts/full_image_content' ); ?>
                
                            <?php } else if ( get_row_layout() == 'block_section_with_no_background_image' ) { //Block section with no background image ?>
                
                                <?php get_template_part( 'template-parts/text_block' ); ?>
                           
                            <?php } else if ( get_row_layout() == 'multiple_columns' ) { //Block section with no background image ?>
                
                                <?php get_template_part( 'template-parts/multiple_columns' ); ?>
                
                            <?php } else if ( get_row_layout() == 'display_posts' ) { // Display posts/shortcode section ?>
                
                                <?php get_template_part( 'template-parts/display_posts' ); ?>
                
                            <?php } else if ( get_row_layout() == 'split_column_image' ) { //Split column layout ?>
                
                                <?php get_template_part( 'template-parts/split_column' ); ?>
                
                            <?php } else if ( get_row_layout() == 'gallery' ) { //Gallery section ?>
                            
                                <?php get_template_part( 'template-parts/gallery' ); ?>
                            
                            <?php } else if ( get_row_layout() == 'gallery_slider_inc_slider' ) { //Gallery section ?> 
                            
                                <?php get_template_part( 'template-parts/slider-links' ); ?>
                                
                            <?php } else if ( get_row_layout() == 'gallery_slider' ) { //Gallery section ?> 
                            
                                <?php get_template_part( 'template-parts/slider' ); ?>
                                
                            <?php } else if ( get_row_layout() == 'tour_dates' ) { //Gallery section ?> 
                            
                                <?php get_template_part( 'template-parts/tour_dates' ); ?>
                            
                            <?php } else if ( get_row_layout() == 'full_video_no_text' ) { //Gallery section ?> 
                            
                                <?php get_template_part( 'template-parts/full_video' ); ?>
                            
                            <?php } ?>
                            
                            <?php wp_reset_postdata();?>
                            
                        <?php } ?>
                    <?php } ?>
                    <!-- end of repeater -->
                    
         	<?php } ?>
      	<?php } ?>
        </div>
<?php get_footer();?>