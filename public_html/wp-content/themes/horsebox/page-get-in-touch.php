<?php
/**
 * Index Template
 * @file           page-get-in-touch.php
 * @package        Horsebox Services
 * @filesource     wp-content/themes/horseboxservices/page-get-in-touch.php
 * @since          Horsebox Services 1.0
*/
get_header();?>
	<div class="body wrapper">  
		<?php if (have_posts()) { ?>
            <?php while (have_posts()) { ?>
            	<?php the_post();?>
                    <?php $lat = get_field('lat','options');?>
                    <?php $long = get_field('longitude','options');?>
                    <?php $address = get_field('company_address','options'); ?>
                    <?php $addresssan = sanitize_text_field( $address );?>
                    <div class="content">
                        <article class="page">
                            <h1 class="page-title"><a class="page-link" rel="bookmark" href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                            	<div class="flexwrapper">
                                	<div class="even block">
                                		<div class="box">
                                        	<div class="center">
												<?php the_content();?>
                                            </div>
                                        </div>
                                  	</div>
                                   	<div class="even block">
                                    	<div class="box">
                                            <div class="companyinfo">
                                            <?php if (get_field('copyright_name','options')) { ?>
                                                <p><a href="<?php echo get_home_url(); ?>" rel="home"><?php the_field('copyright_name','options');?></a></p>
                                            <?php } ?>
                                            <?php if (get_field('company_number','option') ) { ?>
                                                <p><strong>Company No:</strong> <?php the_field('company_number','option');?><br/></p>
                                            <?php } ?>
                                            <?php if (get_field('company_address','option') ) { ?>
                                                <p><i class="fa fa-map-marker" aria-hidden="true"></i><span class="value"><?php the_field('company_address','option');?></span></p>
                                            <?php } ?>
                                                <?php if (get_field('contact_number','option')) { ?>
                                                <?php $phone = get_field('contact_number','option'); ?>
                                                <?php $countryCode = '44'; ?>
                                                <?php $tel = preg_replace("/[^0-9]/", "", $phone); ?>
                                                <?php $tel = preg_replace('/^0?/', '+'.$countryCode, $tel); ?>
                                                <p><a class="phone" href="tel:<?php echo $tel ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span class="value"><?php the_field('contact_number','option') ?></span></a></p>
                                            <?php } ?>
                                            <?php if (get_field('contact_email','option')) { ?>
                                                <p><a class="email" href="mailto:<?php the_field('contact_email','option') ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <span class="value"><?php the_field('contact_email','option') ?></span></a></p>
                                            <?php } ?>
                                            </div>
                                        </div>
                                	</div>
                               	</div>
                        </article>
                    </div>
                        <section class="maps">
                            <div class="mapwrapper">
                            	<div class="mapdirections">
                                    <div class="content">
                                    <form action="http://maps.google.com/maps" method="get" target="_blank" class="">
                                       <h3 class="subheading">Looking for directions?</h3>
                                       <label for="saddr" class="screen-reader-text">Enter your postcode for directions</label>
                                       <input type="text" name="saddr" placeholder="Your Location" class="field"/>
                                       <input type="hidden" name="daddr" value="<?php echo $addresssan; ?>" />
                                      <button type="submit" value="Get directions" class="button">
                                        <span class="leftborder buttontext">Get Directions</span><span class="rightborder buttonicon"><i class="fas fa-map-marker"></i></span>
                                      </button>

                                       </p>
                                    </form>
                                </div>
                                </div>
                                <div id="map"></div>
                            </div>
                                    <script>
                                      var map;
                                      function initMap() {
                                        var myLatLng = {lat:<?php echo $lat ?>, lng:<?php echo $long ?>}
                                        var map = new google.maps.Map(document.getElementById('map'), {
                                          center: myLatLng,
                                          zoom: 14,
                                          scrollwheel:  false,
                                          panControl: false,
                                          disableDefaultUI: true,
                                          draggable: false
                                        });
        /*                                                  var image = {
                                              url: 'http://stpeter.fifteenit.co.uk/wp-content/uploads/2017/03/map-pin.png',
                                              size: new google.maps.Size(82,113),
                                              origin: new google.maps.Point(0,0),
                                              anchor: new google.maps.Point(-50,225)
                                          }*/
                                          var marker = new google.maps.Marker({
                                            position: myLatLng,
                                            map: map,
                                            //icon: image,
                                            title: 'Fifteen IT HQ'
                                        });
                                      }
                                    </script>
                                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2O_Ah9Wj63czdaI8_XkkLswxl_v85o7o&callback=initMap"
                                    async defer></script>
                        </section>
            <?php } ?>
        <?php } ?>    
	</div>
<?php get_footer();?>