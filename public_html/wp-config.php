<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'horseboxservices_db');


/** MySQL database username */
define('DB_USER', 'horseboxservices_ur');


/** MySQL database password */
define('DB_PASSWORD', 'm[u6!I%m[Hp%');


/** MySQL hostname */
define('DB_HOST', 'localhost');


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');


/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rz[NfC{eDk]nd<B6NAgW;IK?_vY4J)89g0rMlx>@`4:%ebsE$6K}b#)yt!Tsh2F2');

define('SECURE_AUTH_KEY',  'W>/YYd#`t{Q83,Dm{G+6e(dM(}t0D[$%O=A{pT)0Az~_q!YIU&jom]vObNcuO%2a');

define('LOGGED_IN_KEY',    'o_yA3KXRi.nA1+s7buQG8ih8SK[`[Ht.cdK^Ig@^QvM`lq4&~-&jV i0lLBQE!c%');

define('NONCE_KEY',        '592/PMuO4?R|VmAWrdhjl7BF}!cg*:.~9)-1%$qp(7?VaECqJsJr:cA5xqLi8pd#');

define('AUTH_SALT',        'G1A-OCvE KzE(%?^KhpY7mL2E^GRG:oqEo Hcr.=rgA0;LA?M=%/O#gptS_f}l^b');

define('SECURE_AUTH_SALT', '*f+SyYu1*9;K.)a8ByRJeh,+Hoq#I1r<!c[MF;KaW)S%xMj+L2re9h30lJ5:7qa`');

define('LOGGED_IN_SALT',   'gA==6Y5:]^SnU?R?~HhPfw3+/Qr#~rlTEe5)%rCx^9m=v$mE+*la5e)R})4z=Ym}');

define('NONCE_SALT',       '.#VX04:up_477hwD*vTQo>xM,94+o&I|W^7-%QLO(Y%c2xMVlF$emrMu_J-I:m.M');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
